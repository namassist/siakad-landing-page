/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./**/*.{html,js}"],
  theme: {
    extend: {
      container: {
        padding: {
          DEFAULT: "1rem",
          sm: "2rem",
          lg: "4rem",
          xl: "5rem",
          "2xl": "6rem",
        },
      },
      colors: {
        transparent: "transparent",
        primary: "#22293E",
        secondary: "rgba(34, 41, 62, 0.60)",
        third: "rgba(34, 41, 62, 0.80)",
        white: "#F8FCFD",
      },
    },
  },
  plugins: [],
};
